package com.example.isai;

import android.os.Bundle;
import android.media.MediaMetadataRetriever;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugins.GeneratedPluginRegistrant;
import java.util.*;

public class MainActivity extends FlutterActivity {
  static final String MY_CHANNEL = "imirNativeService";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);
    new MethodChannel(getFlutterView(), MY_CHANNEL).setMethodCallHandler(new MethodCallHandler() {
      @Override
      public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("getTags")) {
          String argumentPassed = call.arguments();
          result.success(getSongDetails(argumentPassed));
        }
      }
    });
  }

  public Map<String, Object> getSongDetails(String filePath) {
    MediaMetadataRetriever metaData = new MediaMetadataRetriever();
    Map<String, Object> fileTags = new HashMap<String, Object>();
    byte[] imageBytes;
    try {
      metaData.setDataSource(filePath);
      fileTags.put("title", metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
      fileTags.put("artist", metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
      fileTags.put("albumArtist", metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST));
      fileTags.put("albumArt", metaData.getEmbeddedPicture());
      fileTags.put("album", metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
      fileTags.put("duration", Long.valueOf(metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)));
      fileTags.put("year", Long.valueOf(metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR)));
      fileTags.put("composer", metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_COMPOSER));
      fileTags.put("author", metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR));
      fileTags.put("genre", metaData.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
      return fileTags;

    } catch (Exception e) {
      throw e;
    }
  }
}
