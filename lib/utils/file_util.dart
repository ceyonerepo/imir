import 'dart:io';
import '../models/song.dart';
import 'package:flutter/services.dart';
import 'dart:core';

String curDirectory = "";
List<String> audioFormats = ["mp3"];

/* Class to handle file opeations */
class FileHandler {
  static const platform = const MethodChannel("imirNativeService");
  List<Song> songs = new List();

  /*This function is used to list directories/subdirectories
  * @required root or parent directory path
  * [isRecursive = true] optional parameter to check the sub directories, be default is true */
  static List getDirectoryPaths(String path, {bool isRecursive = true}) {
    List directoriesPath = new List();
    try {
      Directory rootDirectory = new Directory(path);
      if (rootDirectory.existsSync()) {
        for (FileSystemEntity fileEntity
            in rootDirectory.listSync(recursive: isRecursive)) {
          //Check is curDirectory and previous directory are same
          if (fileEntity is Directory) {
            curDirectory = (curDirectory == fileEntity.path
                ? curDirectory
                : fileEntity.path);
          }
          // Check if the file is mp3 file
          if (fileEntity is File &&
              isAudioFile(fileEntity) &&
              !directoriesPath.contains(curDirectory)) {
            directoriesPath.add(curDirectory);
          }
        }
      }
    } catch (e) {
      print(e);
    }
    return directoriesPath;
  }

  /*This function is used to getting songs from a directory
  * @required directory path 
  * [isRecursive = true] optional parameter to check the sub directories, be default is true */
  static Future getSongs(String path, {bool isRecursive: true}) async {
    try {
      List<Song> songs = new List();
      var curTags;
      final Directory fileDirectory = new Directory(path);
      if (fileDirectory.existsSync()) {
        for (FileSystemEntity fileEntity
            in fileDirectory.listSync(recursive: isRecursive)) {
          if (isAudioFile(fileEntity)) {
            curTags = await getSongTags(fileEntity.path);
            songs.add(curTags);
          }
        }
      }
      return songs;
    } catch (e) {
      print(e);
    }
  }

  /*This function is used to check whether the file is supported audio file or not
  * @required file */
  static bool isAudioFile(file) {
    bool isAudioFile = false;
    if (file is File && audioFormats.contains(file.path.split(".").last)) {
      isAudioFile = !isAudioFile;
    }
    return isAudioFile;
  }

  /* This function is used to getting songs ID3 Tags
  *  @required file path */
  static Future<dynamic> getSongTags(String path) async {
    final Map<dynamic, dynamic> contents =
        await platform.invokeMethod("getTags", path);

    String _title = contents["title"];
    String _album = contents["album"];
    String _composer = contents["composer"];
    String _artist = contents["artist"];
    String _albumArtist = contents["albumArtist"];
    String _author = contents["author"];
    String _genre = contents["genre"];
    int _duration = contents["duration"];
    int _year = contents["year"];
    String _path = path;
    List<dynamic> _albumArt = contents["albumArt"];
    return Song(_title, _album, _composer, _artist, _albumArtist, _author,
        _genre, _duration, _year, _path, _albumArt);
  }
}
