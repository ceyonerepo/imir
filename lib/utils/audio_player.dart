import 'package:audioplayers/audioplayers.dart';

class ImirAudio {
  AudioPlayer imirPlayer = new AudioPlayer();

  Future playSong(song) async {
    int results = await imirPlayer.play(song, isLocal: true);
    if (results == 1) {
      print("song is playing");
    } else {
      print("Song is not Playing");
    }
  }
}
