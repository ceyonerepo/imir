import 'package:flutter/material.dart';
import './imir_home_page.dart';

void main() {
  runApp(MaterialApp(
    title: "Imir Music Player",
    home: ImirHomePage(),
    showSemanticsDebugger: false,
    debugShowCheckedModeBanner: false,
  ));
}
