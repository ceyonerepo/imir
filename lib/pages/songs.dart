import 'package:flutter/material.dart';

class DirectorySongs extends StatelessWidget {
  final List songsList;
  DirectorySongs({Key key, this.songsList}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: songsList.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title: songsList[index],
          leading: Icon(Icons.music_note),
          trailing: Icon(Icons.more_vert),
        );
      },
    );
  }
}
