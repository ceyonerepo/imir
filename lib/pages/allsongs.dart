import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';
import '../utils/file_util.dart';
import '../models/song.dart';
import '../widgets/loading_widget.dart';
import 'package:path_provider/path_provider.dart';

class AllSongs extends StatefulWidget {
  @override
  _AllSongsState createState() => _AllSongsState();
}

class _AllSongsState extends State<AllSongs> {
  String path = "/sdcard";
  List filePaths = new List();
  List fileTags = new List();
  double progressValue = 0.2;

  final AsyncMemoizer _memoize = new AsyncMemoizer();

  @override
  void initState() {
    super.initState();
  }

  /*
    This function is used to getsongs from the root directory
  */
  Future _getSongs() async {
    await requestPerm(); // Requestiong permission to read and write files
    final extDir = await getExternalStorageDirectory();
    //Memoizing getSongs result feature to avoid rebuild on tab navigations
    return _memoize.runOnce(() async {
      List<Song> songs = await FileHandler.getSongs(extDir.path);
      return songs;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getSongs(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data != null) {
          return Container(
            child: Center(
              child: ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(snapshot.data[index].title),
                    contentPadding: EdgeInsets.only(left: 5.0),
                    subtitle: Text(snapshot.data[index].album),
                    leading: CircleAvatar(
                      radius: 30.0,
                      backgroundImage:
                          MemoryImage(snapshot.data[index].albumArt),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: null,

                    //ImirAudio().playSong(filePaths[index].path); //Will initialize the audio player, and play the song

                    onLongPress: () {
                      print(snapshot.data[index]);
                    },
                  );
                },
              ),
            ),
          );
        } else {
          return getLoadingWidget('Fetching Songs');
        }
      },
    );
  }

  /*
    Future to request the permission to read and write files
  */
  Future<void> requestPerm() async {
    await SimplePermissions.requestPermission(Permission.ReadExternalStorage);
  }
}
