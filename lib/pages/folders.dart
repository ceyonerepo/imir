import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';
import '../utils/file_util.dart';

class FoldersPage extends StatefulWidget {
  @override
  _FoldersPageState createState() => _FoldersPageState();
}

class _FoldersPageState extends State<FoldersPage> {
  int test = 0;
  String path = "/sdcard";
  List directoryPaths = new List();

  @override
  void initState() {
    super.initState();
    requestPerm();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Folders"),
      ),
      body: Container(
        child: Center(
          child: ListView.builder(
            itemCount: directoryPaths.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(directoryPaths[index]),
                leading: Icon(Icons.folder),
                onTap: () {},
              );
            },
          ),
        ),
      ),
    );
  }

  Future<void> requestPerm() async {
    final res = await SimplePermissions.requestPermission(
        Permission.ReadExternalStorage);
    print(res.toString());
    setState(() {
      directoryPaths = FileHandler.getDirectoryPaths(path);
    });
  }
}
