import 'dart:typed_data';

class Song {
  String title;
  String album;
  String composer;
  String artist;
  String albumArtist;
  String author;
  String genre;
  int duration;
  int year;
  String path;
  List<dynamic> albumArt;

  Song(
    this.title,
    this.album,
    this.composer,
    this.artist,
    this.albumArtist,
    this.author,
    this.genre,
    this.duration,
    this.year,
    this.path,
    this.albumArt,
  );
}
