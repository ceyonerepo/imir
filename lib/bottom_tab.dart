import 'package:flutter/material.dart';

class ImirBottomNavigation extends StatefulWidget {
  @override
  _ImirBottomNavigationState createState() => _ImirBottomNavigationState();
}

class _ImirBottomNavigationState extends State<ImirBottomNavigation> {
  var _selectedPage = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => BottomNavigationBar(
          currentIndex: _selectedPage,
          onTap: (int index) {
            setState(() {
              _selectedPage = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.music_note), title: Text("All Songs")),
            BottomNavigationBarItem(
                icon: Icon(Icons.playlist_play), title: Text("Playlists")),
            BottomNavigationBarItem(
                icon: Icon(Icons.queue_music), title: Text("Now Playing"))
          ]);
}
