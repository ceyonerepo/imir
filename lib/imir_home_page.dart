import 'package:flutter/material.dart';
import './pages/allsongs.dart';
import './pages/folders.dart';
import './pages/nowplaying.dart';

class ImirHomePage extends StatefulWidget {
  @override
  _ImirHomePageState createState() => _ImirHomePageState();
}

class _ImirHomePageState extends State<ImirHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Imir Music Player"),
      ),
      body: Container(
        child: AllSongs(),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        shape: CircularNotchedRectangle(),
        notchMargin: 5.0,
        child: Container(
          height: 50.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            verticalDirection: VerticalDirection.up,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.album,
                  color: Colors.white,
                  size: 30.0,
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FoldersPage()));
                },
              ),
              IconButton(
                  icon: Icon(
                    Icons.playlist_play,
                    color: Colors.white,
                    size: 30.0,
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => NowPlaying()));
                  }),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print("Action button pressed");
        },
        child: Icon(Icons.play_arrow),
        isExtended: true,
      ),
    );
  }
}
