import 'package:flutter/material.dart';

Widget getLoadingWidget(String content) {
  return Container(
    child: Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(
          strokeWidth: 2.0,
          valueColor: AlwaysStoppedAnimation(Colors.grey),
        ),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
          child: Text(
            content,
            style: TextStyle(color: Colors.blue, fontSize: 13.0),
          ),
        ),
      ],
    )),
  );
}
